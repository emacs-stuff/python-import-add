;;;; vindarel 2014-2017
;;;; copyleft

;; Call M-x my-python-import-add RET os sys datetime foo RET and it will:
;; - insert the corresponding "import XXX" statements
;; - sort them.
;; You need pip install isort and package-install py-isort.
;; See also red4e for more python code manipulation.

(require 'dash)
(require 'm-buffer)
(require 's)
(require 'py-isort)

(defun my-insert-import (to_insert)
  (newline)
  (if (or (s-starts-with? "from" (s-trim to_insert))
          (s-starts-with? "import" (s-trim to_insert)))
      (insert (s-trim to_insert))
    (insert "import " to_insert)))

(defvar py-isort-options '("--force_single_line_imports")
  "Options for the isort command.")
;; (setq py-isort-options '("-a hello_foo"))   ;; much simpler !!!
 ;; -a ADD_IMPORTS, --add_import ADD_IMPORTS
                        ;; Adds the specified import line to all files,
                        ;; automatically determining correct placement.

;;TODO: too simple… doesn't work if we have first a docstring with "import" in it. Should skip it.
(defun my-python-import-add (to_import)
  "Adds an import statement for every given module. Can give a comma-separated list of modules, like:
    M-x my-python-import-add RET os, sys, from foo import bar, re RET
  Suggests the word at point.
  "
  (interactive (list (read-from-minibuffer (concat "what to import ? ["
                                                   (thing-at-point 'word)
                                                   "] "))))

  (let* ((to_import (if (string-equal to_import "")
                       (thing-at-point 'word)
                     to_import))
        (to_insert (s-split "/" to_import)))

    (save-excursion  ;; save-restriction + widen ? save-match-data ?
      (beginning-of-buffer)
      (search-forward-regexp "import [a-z_]+$")  ;; if not found ?
      (end-of-line)
      ;; insert each elt of the list:
      (-map 'my-insert-import to_insert)
      ;; sort the imports: (set your py-isort options. See isort -h)
      (py-isort-buffer))))

(defun py-utils-current-line ()
  "Return the current line."
  (let* ((beg (line-beginning-position))
         (end (line-end-position))
         (current-line (buffer-substring-no-properties beg end)))
    current-line))

(defun current-line-indentation ()
  "returns the str of the current indentation (spaces)."
  ;; https://github.com/magnars/s.el#s-match-strings-all-regex-string
  (car (car (s-match-strings-all "^\s+" (py-utils-current-line)) ) )
  )


(defun py-utils-ipdb-cleanup ()
  "Remove all ipdb traces in current buffer."
    (interactive)
    (save-excursion
      (replace-regexp ".*ipdb.set_trace().*\n" "" nil (point-min) (point-max))
      ))

(defhydra py-utils-hydra (:color red :columns 3)
  "Python utils"
  ("i" my-python-import-add "insert import lines")
  ("c" py-utils-ipdb-cleanup "cleanup ipdb traces")
  )

(defalias 'hydra-py-utils 'py-utils-hydra)
